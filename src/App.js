import React, { Component } from "react";
import Form from "react-jsonschema-form";
import "bootstrap/dist/css/bootstrap.min.css";

export default class MyForm extends Component {
  state = {
    tagged: false,
    message: "",
    input1: "",
    input2: ""
  };
  mySchema2 = {};
  mySchema = {
    title: "A Registration Form",
    description: "A simple form example.",
    type: "object",
    required: ["firstName", "lastName"],
    properties: {
      firstName: {
        type: "string",
        title: "First name",
        default: "Souvick"
      },
      lastName: {
        type: "string",
        title: "Last name",
        default: "Samanta"
      },
      age: {
        type: "integer",
        title: "Age"
      },
      bio: {
        type: "string",
        title: "Bio"
      },
      password: {
        type: "string",
        title: "Password",
        minLength: 3
      },
      telephone: {
        type: "string",
        title: "Telephone",
        minLength: 10
      }
    }
  };

  handleInputChange = (e, name) => {
    this.setState({
      [name]: e.target.value
    });
    this.mySchema = JSON.parse(e.target.value);
    console.log(this.mySchema);
  };
  constructor(props) {
    super(props);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit({ formData }) {
    console.log(formData);
  }

  render() {
    return (
      <div class="container">
        <div class="row">
          <div class="col-md">
            <p>Type JSON here now</p>
            <textarea
              class="form-control"
              onChange={e => this.handleInputChange(e, "input1")}
              rows="50"
            >
              {JSON.stringify(this.mySchema, null, 2)}
            </textarea>
          </div>
          <div class="col-md">
            <Form schema={this.mySchema} onSubmit={this.handleSubmit} />
          </div>
        </div>
      </div>
    );
  }
}
